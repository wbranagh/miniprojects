README.txt

This is a collection of small projects related to data analysis or any other topic that I'm currently working on. The code is free for use for any purpose. Currently the projects are:
 
- gpsproject - WiFi and location tracker to investigate the number and type of wiFi connections 
	via an android smart phone. Summaries are available on a blog: www.wbranagh.com 


Let me know if you have questions, comments or suggestions - excluding spam.

Cheers,

Wayne.
